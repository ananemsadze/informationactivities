package com.example.activitiesinformation

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
      val firstName  = intent.extras?.getString("first_name", "" )
        val lastName  = intent.extras?.getString("last_name", "" )
        val age = intent.extras?.getInt("age", 0)


        FirstNameTextView.text = firstName
        LastNameTextView.text = lastName
        AgeTextView.text = age.toString()

        ReturnAdressButton.setOnClickListener(){
            returnAddress()

        }
    }

    private fun returnAddress(){
        val address = AddressEditText.text.toString()
        val intent = intent
        intent.putExtra("address", address)
        setResult(RESULT_OK, intent)
        finish()

    }
}