package com.example.activitiesinformation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {
    private val addressRequestCode = 99

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }

    private fun init(){
        OpenButton.setOnClickListener(){
           opensecondactivity()
        }
    }

    private fun opensecondactivity(){
        val firstname = FirstNameEditText.text.toString()
        val lastname = LastNameEditText.text.toString()
        val age = AgeEditText.text.toString().toInt()


        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("first_name", firstname)
        intent.putExtra("last_name", lastname)
        intent.putExtra("age", age)
        startActivityForResult(intent, addressRequestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode== addressRequestCode && resultCode == RESULT_OK){
            val address = data?.extras?.getString("address", "")
            AddTextView.text = address

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}